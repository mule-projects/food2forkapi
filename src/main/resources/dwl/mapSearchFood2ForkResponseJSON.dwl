%dw 2.0
output text/json skipNullOn="everywhere"

---
{
	count : payload.count,

	recipes: payload.recipes map ((data,index) -> (
		
	if(((data pluck $) joinBy "-" contains /(?i)egg/) == false) data else null 

))
}