**Search and Get eggless recipes **

From Food2Fork Api we can get eggless recipes by providing the recipe Id or
can search recipes by giving the list of ingredients in the response
---

## Get a recipe from Food2Fork Api

1. Provide **recipeId** in the query param.
2. The response gives the information about the recipe with ingredients.
3. The Response will throw error if the ingredients contains egg.


---

## Search a recipe with ingredients from Food2Fork Api

Query Parameters to pass:
1. Pass a **query** with ingredients which are comma separated.
2. **sort**: r(rating) or t(trending)  .
3. **page**:  Any request will return a maximum of 30 results. To get the next set of results send the same request again but with page = 2
              The default if omitted is page = 1 

**For each api call provide client_id and client_secret in headers**